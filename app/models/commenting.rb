
# The Commenting join model. This model is automatically generated and added to your app if you run the commenting generator.

class Commenting < ActiveRecord::Base 
 
  belongs_to :comment
  belongs_to :commentable, :polymorphic => true
  
  # This callback makes sure that an orphaned <tt>Comment</tt> is deleted if it no longer tags anything.
  def before_destroy
    comment.destroy_without_callbacks if comment and comment.commentings.count == 1
  end    
end
