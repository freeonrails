class Article < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :title, :body, :user_id
  
  def self.per_page
    10
  end
  
end
