# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def header_link_to(name, options = {}, html_options = {})
    is_current = current_page?(options)
    
    html_options[:id] = 'current' if is_current
    html = is_current ? '<li id="active">' : '<li>'
    html << link_to(name, options, html_options)
  end
  
  def sign_up_link
    header_link_to("Sign up", new_user_path) unless logged_in?
  end
  
  def log_inout_link
    logged_in? ? header_link_to("Logout", session_path, :method => :delete) : header_link_to("Login", new_session_path)
  end
  
  def user_link(user)
    link_to user.login, user_path(user)
  end
end
