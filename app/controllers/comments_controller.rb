class CommentsController < ApplicationController

  def create
    @article = Article.find(params[:article_id])
    @comment = Comment.new(params[:comment])
    
    if @comment.save
      @article.comments << @comment
      @comment = Comment.new
      flash.now[:notice] = "Thank you for your comment"
    end
    render :template => 'articles/show'
  end

end
