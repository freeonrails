require File.dirname(__FILE__) + '/../test_helper'

class CommentsControllerTest < ActionController::TestCase
  
  def test_posting_comment
    article = Article.find(:first)
    article.comments.destroy_all
    
    assert_difference('Comment.count') do
      post :create, :article_id => article, :comment => { :name => "lifo", :email => "what@ever.com", :body => "foo!"}
      assert_response :success
    end
    
    assert_equal 1, assigns(:article).comments.count
  end
end
