require File.dirname(__FILE__) + '/../test_helper'

class ArticlesControllerTest < ActionController::TestCase
  def setup
    super
    login_as(:quentin)
    users(:quentin).is_admin
  end
  
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_article
    assert_difference('Article.count') do
      post :create, :article => { :title => "foo", :body => 'bar', :user_id => User.find(:first) }
    end
    assert_redirected_to article_path(assigns(:article))
  end

  def test_should_show_article
    get :show, :id => 1
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => 1
    assert_response :success
  end

  def test_should_update_article
    before = Article.find(1).title
    put :update, :id => 1, :article => { :title => "#{before}_changed" }
    after = Article.find(1).title
     
    assert_not_equal before, after
    assert_equal after, "#{before}_changed"
    assert_redirected_to article_path(assigns(:article))
  end

  def test_should_destroy_article
    assert_difference('Article.count', -1) do
      delete :destroy, :id => 1
    end
    assert_redirected_to articles_path
  end
  
  def test_non_admin_user_cannot_create_or_update
    login_as(:aaron)
    
    assert_no_difference('Article.count') do
      post :create, :article => { :title => "foo", :body => 'bar', :user_id => User.find(:first) }
      assert_redirected_to articles_path
    end
    
    get :edit, :id => 1
    assert_redirected_to articles_path
    
    put :update, :id => 1, :article => { }
    assert_redirected_to articles_path
  end
end
