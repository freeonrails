require File.dirname(__FILE__) + '/../test_helper'

class CommentingTest < Test::Unit::TestCase
  fixtures :commentings, :comments, :articles
  def setup
    @obj1 = Article.find(1)
    @obj2 = Article.find(2)
    @comment1 = Comment.find(1)  
    @comment2 = Comment.find(2)  
    @commenting1 = Commenting.find(1)
  end

  def test_commentable
    assert_raises(RuntimeError) do 
      @commenting1.send(:commentable?, true) 
    end
    assert !@commenting1.send(:commentable?)
    
  end
    
end
