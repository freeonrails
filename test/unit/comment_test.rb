require File.dirname(__FILE__) + '/../test_helper'

class CommentTest < Test::Unit::TestCase
  fixtures :comments, :commentings, :articles
  def test_to_s
    assert_equal "no1@nowhere.com", Article.find(2).comments.first.email
    assert_equal "http://letrails.cn", Article.find(2).comments.last.url
    assert_equal "http://fr.ivolo.us", Article.find(2).comments.first.url
  end
  
end
