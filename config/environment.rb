RAILS_GEM_VERSION = '2.0.1' unless defined? RAILS_GEM_VERSION

# Application config variables
CONFIG = {}

# 'authorization' plugin asks for this kinda stuff
AUTHORIZATION_MIXIN = 'object roles'
DEFAULT_REDIRECTION_HASH = {:controller => 'articles', :action => 'index'}
STORE_LOCATION_METHOD = :store_location

require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  config.frameworks -= [ :active_resource ]
  
  config.action_controller.session = {
    :session_key => '_freeonrails_session',
    :secret      => 'dbf3325bdc1fa6c5ce1d23904f7b13db378cac2f283f71075c4e81cce8c09d7e6be8a5c4374776406c58d2660f073aa6adf54a7a7c0bd8d834351eff587575f0'
  }

  config.active_record.observers = :user_observer
  config.active_record.default_timezone = :utc
end

def log_to(stream=$stdout)
  ActiveRecord::Base.logger = Logger.new(stream)
  ActiveRecord::Base.clear_active_connections!
end

require 'commenting_extensions'
